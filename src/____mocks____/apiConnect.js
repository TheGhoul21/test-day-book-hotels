export const apiLoading = {
  isLoading: jasmine.createSpy().and.returnValue(true),
  exists: jasmine.createSpy().and.returnValue(false)
}

export const apiLoaded = {
  isLoading: jasmine.createSpy().and.returnValue(false),
  exists: jasmine.createSpy().and.returnValue(true)
}


export const apiError = {
  isLoading: jasmine.createSpy().and.returnValue(false),
  exists: jasmine.createSpy().and.returnValue(false)
}
