import { Map, fromJS } from 'immutable';
var data = Map();
export default data;
var timeMap = Map();

const MAX_AGE = 100 * 1000;
export function addData(path, _data) {
  var splitPath = cleanSplitPath(path);
  data = data.setIn(splitPath, Map(_data));
  if(_data.join) {
    _data.map((item) => {
      timeMap = timeMap.setIn([path + item.id], +new Date);
    })
  } else {
    timeMap = timeMap.setIn([path], +new Date);
  }

}

export function getData(path) {
  var splitPath = cleanSplitPath(path);
  return (timeMap.has([path])
    && timeMap.getIn([path])  + MAX_AGE < +new Date)
    ? data.getIn(splitPath).toJS()
    : null
}


function cleanSplitPath(path) {
  return path.replace(/^\//, "").split("/").map((i) => "" + i);
}
