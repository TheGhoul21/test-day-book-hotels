import axios from 'axios';
import { API_DATA_FETCHED, API_DATA_ERROR, API_DATA_LOADING } from './apiActionTypes';

var instance = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/',
  timeout: 1000,
});

export async function fetchData(path) {
  try {


    await timeout(0); // simulate timewout to show loading status
    /**
     * consider using here a cache layer
     * it should check on multiple things:
     * # nested data
     * # MAX_AGE of data with force reload after a while
     * option to skip cache
     * API to manage cache (clear data, clear single entry etc)
     * see src/api/_______________________cachedData.js for a basic API 
     */
    const response = await instance.get(path);

    return {
      type: API_DATA_FETCHED,
      data : response.data,
      path
    }

  } catch(error) {
    return {
      type: API_DATA_ERROR,
      path,
      error,
    }
  }
}

export function loadingData(path) {
  return {
    type: API_DATA_LOADING,
    path
  }
}


const timeout = (time) =>
  new Promise(resolve => setTimeout(_ => resolve("theValue"), time));
