import React, { Component } from 'react';
import PropTypes from 'prop-types'
import { fetchData, loadingData } from './apiActions';
import { fromJS } from 'immutable';
import { ____LOADING____, ____NOT_FOUND____ } from './apiReducers';

export default function(callback){
  return function(WrappedComponent) {
    return class ApiComponent extends Component {
      static displayName = `APIConnect(${getDisplayName(WrappedComponent)})`
      static contextTypes = {
        store: PropTypes.object.isRequired
      };
      static propTypes = {
        paths: PropTypes.func
      }

      state = {
        data: {}
      }


      addPath = (paths) => {
        const { store } = this.context;

        if(!paths.join) {
          paths = [paths]
        }

        for(var index in paths) {
          store.dispatch(loadingData(paths[index]));
          store.dispatch(fetchData(paths[index]))
        }
      }

      isLoading = (obj) => {
        return obj && obj.hasOwnProperty(____LOADING____);
      }
      exists = (obj) => {
        return obj && !obj.hasOwnProperty(____NOT_FOUND____);
      }
      componentDidMount() {
        const { store } = this.context;
        const paths = callback(this.props);
        for(var index in paths) {
          store.dispatch(loadingData(paths[index]));
          store.dispatch(fetchData(paths[index]))
        }
      }

      render() {
        return (<WrappedComponent api={{addPath: this.addPath, exists: this.exists, isLoading: this.isLoading }} {...this.props} />)
      }
    }
  }
}


function getDisplayName(WrappedComponent) {
  return WrappedComponent.displayName ||
         WrappedComponent.name ||
         'Component'
}

export function cleanSplitPath(path) {
  return path.replace(/^\//, "").split("/");
}

export function dataToJs(state, path) {
  path = cleanSplitPath(path);

  state = fromJS(state);
  if(state.hasIn(path)) {
    return state.getIn(path).toJS();
  }

  return undefined;
}

// export function addToDataLoading(path) {
//   path = cleanSplitPath(path);
//   loadingData = loadingData.setIn(path, true);
// }
// export function removeFromDataLoading(path) {
//   path = cleanSplitPath(path);
//   loadingData = loadingData.removeIn(path);
// }
// export function isLoading(path) {
//   path = cleanSplitPath(path);
//   return loadingData.hasIn(path);
// }
