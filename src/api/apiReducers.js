import { API_DATA_FETCHED, API_DATA_ERROR, API_DATA_LOADING } from './apiActionTypes';
import { fromJS } from 'immutable';
export const ____LOADING____ = "____LOADING____";
export const ____NOT_FOUND____ = "____NOT_FOUND____";

const initialState = {

}

export default function apiReducer(state = initialState, action) {
  state = fromJS(state);
  var path;
  switch(action.type) {
    case API_DATA_FETCHED:
      path = action.path.replace(/^\//, "").split("/");
      var data = action.data;

      if(data.join) {
        // we have an array, handle it as such
        state = state.removeIn([...path, ____LOADING____])

        data.map((item) => {
          state = state.removeIn([...path, "" + item.id, ____LOADING____])
          state = state.setIn([...path, "" + item.id], item)
          return true;
        })
      } else {
        state = state.removeIn([...path, ____LOADING____])
        if(state.hasIn(path)) {
          const tempData = state.getIn(path).toJS();
          data = {...data, ...tempData };
        }
        state = state.setIn(path, data);
      }
      return state.toJS()
    case API_DATA_ERROR:
      path = action.path.replace(/^\//, "").split("/");
      state = state.setIn(path, { [____NOT_FOUND____] : true});
      return state.toJS()
    case API_DATA_LOADING:
      path = action.path.replace(/^\//, "").split("/");
      state = state.setIn(path, { [____LOADING____] : true});
      return state.toJS();
    default:
      return state.toJS()
  }
}
