import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Welcome to my blog</h2>
        </div>
        <p className="App-intro">
          See a full list of posts <Link to="/posts">here</Link> 
        </p>
      </div>
    );
  }
}

export default App;
