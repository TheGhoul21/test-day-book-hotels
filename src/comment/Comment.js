import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';

class Comment extends Component {
    render() {
      const { comment } = this.props;

      if(!comment || this.props.api.isLoading(comment)) {
        return <h2>Loading...</h2>;
      }

        return (
            <div className="Comment">
              {comment && (<div>
                <p>
                  <b>{comment.name} -
                    &nbsp;<a href={"mailto:" + comment.email}>{ comment.email }</a>:
                  </b> Wrote:
                </p>
                <p>
                  { comment.body }
                </p>
                <Link to={"/posts/" + comment.postId }>Back to post</Link>
              </div>) }
              { comment === null && <h1>COMMENT NOT FOUND</h1> }
            </div>
        );
    }
}

Comment.propTypes = {
  comment: PropTypes.object
}

Comment.defaultProps = {

}

export default Comment
