import React from 'react';
import ReactDOM from 'react-dom';
import Comment from './Comment';
import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import { apiLoaded, apiLoading, apiError } from '../____mocks____/apiConnect';
import { fakePost, fakeUser, fakeComments } from '../____mocks____/data';
import { MemoryRouter, Match } from 'react-router'


describe('Post', () => {
  var ComponentLoaded, ComponentNotLoaded, ComponentNotExists;
  beforeEach(() => {
    ComponentLoaded = mount(<MemoryRouter><Comment api={apiLoaded} comment={fakeComments[0]} /></MemoryRouter>);
    ComponentNotLoaded = mount(<MemoryRouter><Comment api={apiLoading} comment={fakeComments[0]} /></MemoryRouter>);
    ComponentNotExists = mount(<MemoryRouter><Comment api={apiError} comment={fakeComments[0]} /></MemoryRouter>);
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<MemoryRouter><Comment api={apiLoaded} comment={fakeComments[0]} /></MemoryRouter>, div);
    const treeLoaded = renderer.create(
      <MemoryRouter location="/">
        <Comment api={apiLoaded}  post={fakePost} user={fakeUser} />
      </MemoryRouter>
    ).toJSON();
    expect(treeLoaded).toMatchSnapshot();
  });



  it('renders all data if exists', () => {
    expect(ComponentLoaded).toBeTruthy();
    expect(ComponentLoaded.find('h2').length).toBe(0)
  })

  it('renders loading', () => {
    expect(ComponentNotLoaded).toBeTruthy();
    expect(apiLoading.isLoading).toHaveBeenCalledWith(fakeComments[0]);
    expect(ComponentNotLoaded.find('h2').length).toBeTruthy()
    expect(ComponentNotLoaded.find('h2').text()).toMatch("Loading")

  })
})
