import template from './Comment.js'
import { connect } from 'react-redux';
import apiConnect, { dataToJs } from '../api/apiConnect';

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
}


const mapStateToProps = ({ api }, ownProps) => {
    return {
      comment: dataToJs(api, `/comments/${ownProps.match.params.id}`)
    };
};

const Comment = apiConnect(({ match : { params: { id }}}) => ['/comments/' + id])(template);



export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Comment);
