import React from 'react';
import ReactDOM from 'react-dom';
import App from './app/App';
import Posts from './posts';
import Post from './post';
import Comment from './comment';

import registerServiceWorker from './registerServiceWorker';
import { store, history } from './app/store'

import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import { Route } from 'react-router'
import './index.css'
//ReactDOM.render(<App />, document.getElementById('root'));


ReactDOM.render(
  <Provider store={store}>
    { /* ConnectedRouter will use the store from Provider automatically */ }
    <ConnectedRouter history={history}>
      <div>
        <Route exact path="/" component={App}/>
        <Route exact path="/posts" component={Posts} />
        <Route path="/posts/:id" component={Post} />
        <Route path="/comment/:id" component={Comment} />
      </div>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
)

registerServiceWorker();
