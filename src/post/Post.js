import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import './Post.css';

class Post extends Component {
  componentWillReceiveProps(nextProps) {
    const { post } = nextProps;
    const { user } = nextProps
    if(post && post.userId && !user) {
      this.props.loadAuthor(post.userId);
    }

  }
    render() {
      const { post } = this.props;

      if(this.props.api.isLoading(post)) {
        return <h2>Loading...</h2>;
      }

      if(!this.props.api.exists(post)) {
        return <h1>Post not found</h1>;
      }

      const { title, body, comments } = post;
      const { user } = this.props;


      const _comments = comments && Object.values(comments).map((comment) => (
        <div key={comment.id}>
          <p>
            <i className="Email">{ comment.email }</i> |&nbsp;
            <b className="Name">{ comment.name }</b>
          </p>
          <p className="Body">{ comment.body }</p>
          <Link to={"/comment/" + comment.id }>read more...</Link>
          <hr />
        </div>));
        var author;
        if(this.props.api.isLoading(user)) {
          author = <i>Loading...</i>;
        } else if(this.props.api.exists(user)) {
          author =  (
            <div className="Author">
              <h3>About the author:</h3>
              <p className="Username">{ user.username }</p>
              <p><i className="Email">{ user.email }</i></p>
            </div>
          );
        } else {
          author = <b>Not found</b>;
        }

        return (
            <div className="Post">
              <h1>{ title }</h1>
              <p className="body">{ body }</p>
              { author }
              <hr />
              <div className="Comments">{ _comments ? _comments : <i>no comments</i> }</div>
              <Link to="/posts">Back to posts</Link>
            </div>
        );
    }
}

Post.propTypes = {
  post: PropTypes.object
}

Post.defaultProps = {}

export default Post
