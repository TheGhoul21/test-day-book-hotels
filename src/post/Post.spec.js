import React from 'react';
import ReactDOM from 'react-dom';
import Post from './Post';
import { mount } from 'enzyme';
import renderer from 'react-test-renderer';
import { apiLoaded, apiLoading, apiError } from '../____mocks____/apiConnect';
import { fakePost, fakeUser, fakeComments } from '../____mocks____/data';
import { MemoryRouter, Match } from 'react-router'


describe('Post', () => {
  var ComponentLoaded, ComponentNotLoaded, ComponentNotExists;
  beforeEach(() => {
    ComponentLoaded = mount(<MemoryRouter><Post api={apiLoaded} user={fakeUser} post={fakePost} /></MemoryRouter>);
    ComponentNotLoaded = mount(<MemoryRouter><Post api={apiLoading} user={fakeUser} post={fakePost} /></MemoryRouter>);
    ComponentNotExists = mount(<MemoryRouter><Post api={apiError} user={fakeUser} post={fakePost} /></MemoryRouter>);
  });

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<MemoryRouter><Post api={apiLoaded} user={fakeUser} post={fakePost} /></MemoryRouter>, div);
    const treeLoaded = renderer.create(
      <MemoryRouter location="/">
        <Post api={apiLoaded}  post={fakePost} user={fakeUser} />
      </MemoryRouter>
    ).toJSON();
    expect(treeLoaded).toMatchSnapshot();
  });



  it('renders all data if exists', () => {
    expect(ComponentLoaded).toBeTruthy();
    expect(ComponentLoaded.find('h1')).toBeTruthy()
    expect(ComponentLoaded.find('h1').text()).toBe(fakePost.title)
    expect(ComponentLoaded.find('p.body').text()).toBe(fakePost.body)

    expect(ComponentLoaded.find('div.Comments')).toBeTruthy();
    expect(ComponentLoaded.find('div.Comments').children().length).toBe(fakeComments.length);
    expect(ComponentLoaded.find('div.Comments').at(0).find('.Email')).toBeTruthy();
    expect(ComponentLoaded.find('div.Comments').at(0).find('.Email').at(0).text()).toBe(fakeComments[0].email);
    expect(ComponentLoaded.find('div.Comments').at(0).find('.Name')).toBeTruthy();
    expect(ComponentLoaded.find('div.Comments').at(0).find('.Name').at(0).text()).toBe(fakeComments[0].name);
    expect(ComponentLoaded.find('div.Comments').at(0).find('.Body')).toBeTruthy();
    expect(ComponentLoaded.find('div.Comments').at(0).find('.Body').at(0).text()).toBe(fakeComments[0].body);

    expect(ComponentLoaded.find('.Author .Email').text()).toBe(fakeUser.email)
    expect(ComponentLoaded.find('.Author .Username').text()).toBe(fakeUser.username)

  })

  it('renders loading', () => {
    expect(ComponentNotLoaded).toBeTruthy();
    expect(apiLoading.isLoading).toHaveBeenCalledWith(fakePost);
    expect(ComponentNotLoaded.find('h2').length).toBeTruthy()
    expect(ComponentNotLoaded.find('h2').text()).toMatch("Loading")
    expect(ComponentNotLoaded.find('p.body').length).not.toBeTruthy();

    expect(ComponentNotLoaded.find('div.Comments')).toBeTruthy();
    expect(ComponentNotLoaded.find('div.Comments').length).not.toBeTruthy();


    expect(ComponentNotLoaded.find('.Author').length).not.toBeTruthy();
  })
})
