import template from './Post.js'
import { connect } from 'react-redux';
import apiConnect, { dataToJs } from '../api/apiConnect';

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
      loadAuthor(userId) {
        this.api.addPath(`/users/${userId}`);
      }
    }
}

const mapStateToProps = ({ api }, ownProps) => {

  const post = dataToJs(api, `/posts/${ownProps.match.params.id}`); //(api.posts && api.posts[ownProps.match.params.id]) || { } ;

  const user = (post && post.userId && dataToJs(api, `/users/${post.userId}`)) || null;
    return {
      post,
      user: user
      // post: {}
    };
};

const Post = apiConnect(({ match : { params: { id }}}) => ['/posts/' + id, '/posts/' + id +"/comments"])(template);

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Post);
