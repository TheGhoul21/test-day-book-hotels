import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default class PostItem extends Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    body: PropTypes.string.isRequired,
  }
  render() {
    const { title, body, id } = this.props;
    return (
      <div key={id}>
      <h1>{ title }</h1>
      <p>{ body }</p>
      <Link to={"/posts/" + id }>See more...</Link>
      </div>
    )
  }
}
