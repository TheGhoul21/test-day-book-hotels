import React, { Component } from 'react'
import PropTypes from 'prop-types'
import PostItem from './PostItem';

class Posts extends Component {
    render() {
        const { posts } = this.props;

        return (
            <div className="Posts">
              <h1>POSTS:</h1>
              { (!posts || this.props.api.isLoading(posts)) ? <i>Loading</i> : (
                Object.keys(posts).map((index) => (
                  <PostItem key={index} {...posts[index]} />
                )))
              }
            </div>
        );
    }
}

Posts.propTypes = {
  post: PropTypes.object
}

Posts.defaultProps = {}

export default Posts
