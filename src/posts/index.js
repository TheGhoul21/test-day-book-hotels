import template from './Posts.js'
import { connect } from 'react-redux';
import apiConnect from '../api/apiConnect';

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
}

const mapStateToProps = ({ api: { posts } }, ownProps) => {
    return {
      posts
    };
};
const Posts = apiConnect(() => ['/posts'])(template);

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(Posts);
